=====================
SQL CRUD Operations
=====================

1. (Create) Adding a record.
    Terminal
        Adding a record:
        Syntax
            INSERT INTO table_name (column_name) VALUES (value1);

2. (Read)Show all records.
    Terminal

        Displaying/retrieving records:
        Syntax
            SELECT column_name FROM table_name;

3. (Create) Adding a record with multiple columns.
    Terminal

        Adding a record with multiple columns:
        Syntax
            - INSERT INTO table_name (column_name, column_name) VALUES (value1, value2);

            --Note: Values must match the column.

5. (Create) Adding multiple records.
    Terminal

        Adding multiple records:
        Syntax
            INSERT INTO table_name (column_name, column_name) VALUES (value1, value2), (value3, value4);


6. Show records with selected columns.
    Terminal

       Retrieving records with selected columns:
        Syntax
            SELECT (column_name1, column_name2) FROM table_name;

        SELECT song_name, genre FROM songs;

7. (Read) Show records that meet a certain condition.
    Terminal

        Retrieving records with certain condtions:
        Syntax
            SELECT column_name FROM table_name WHERE condition;


8. (Read) Show records with multiple conditions.
    Terminal

        Displaying/retrieving records with multiple conditions:
        Syntax
            AND CLAUSE
                SELECT column_name FROM table_name WHERE condition1 AND condition2;
            OR CLAUSE
                SELECT column_name FROM table_name WHERE condition1 OR condition2;

9. (Update) Updating records.
    Terminal

        Add a record to update:
        INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Megalomaniac", 410, "Classical", 2);

        Updating records:
        Syntax
            UPDATE table_name SET column_name = value WHERE condition;

            UPDATE table_name SET column_name = value, column_name2 = value2 WHERE condition;

            --Note: When updating and deleting, add a where clause or else you may update or delete all items in a table.

10. (Delete) Deleting records.
    Terminal
        Deleting records:
        Syntax
            DELETE FROM table_name WHERE condition;


CRUD Operations

Create

INSERT INTO artists (name) VALUES ('Incubus');
INSERT INTO artists (name) VALUES ("Psy");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Make Yourself","1999-10-26",4);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6","2012-1-15",5);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Second Stage Turbine Blade","2002-3-05",6); 
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Say It Like You Mean IT","2002-7-16",7); 
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Swiss Army Romance","2000-3-01",8); 

For Multiple Insertion
INSERT INTO songs (song_name, length, genre, album_id) 
VALUES ("Pardon Me",223,"Rock",4); 
("Gangnam Style",253,"K-Pop",5); 
("Time Consumer",550,"Acoustic Rock",6); 
("The Best of Me",418,"Acoustic Rock",7); 
("Screaming Infidelities",333,"Acoustic Rock",8); 

Retrieve All

SELECT * FROM artists;

Specific Record Retrieval

SELECT song_name FROM songs;
SELECT album_title, artist_id FROM albums;
SELECT song_name FROM songs WHERE genre = "Rock";

Specific Record Retrieval With Condition

SELECT song_name,genre FROM songs WHERE length > 200;
SELECT song_name FROM songs WHERE genre = "Rock" AND length < 200;


UPDATE

UPDATE songs SET length = 200, genre = "Rock", album_id = 4 Where song_name = "Stellar";

Delete

DELETE FROM songs WHERE genre = "Rock" AND length < 200;


